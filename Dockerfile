FROM node:14

WORKDIR /usr/src/microservice-product-api

COPY ./package.json .

COPY ./dist ./dist 

COPY ormconfig.env .

RUN npm install --only=prod 
