import { Validation } from '../../presentation/validations'
import { RequiredFieldValidation } from '../../presentation/validations/required-field-validate'
import { ValidationComposite } from '../../presentation/validations/validation-composite'

export const makeAddProductValidation = (): ValidationComposite => {
  const validations: Validation[] = []
  for (const field of ['title', 'description', 'figure', 'price']) {
    validations.push(new RequiredFieldValidation(field))
  }
  return new ValidationComposite(validations)
}
