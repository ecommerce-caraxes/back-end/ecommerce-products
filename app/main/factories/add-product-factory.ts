import { DbAddProduct } from '../../data/usecases/add-product/db-add-product'
import { ProductRepository } from '../../infra/db/mysql/product/product-repository'
import { AddProductController } from '../../presentation/add-product/add-product-controller'
import { makeAddProductValidation } from './add-product-validation'

export const makeAddProduct = () => {
  const productRepository = new ProductRepository()
  const addProduct = new AddProductController(
    makeAddProductValidation(),
    new DbAddProduct(productRepository)
  )
  return addProduct
}
