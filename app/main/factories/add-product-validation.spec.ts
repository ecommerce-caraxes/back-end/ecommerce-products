import { Validation } from '../../presentation/validations'
import { RequiredFieldValidation } from '../../presentation/validations/required-field-validate'
import { ValidationComposite } from '../../presentation/validations/validation-composite'
import { makeAddProductValidation } from './add-product-validation'

jest.mock('../../presentation/validations/validation-composite')

describe('AddProductValidation Factory', () => {
  test('Should call ValidationComposite with all validatotions', () => {
    makeAddProductValidation()
    const validations: Validation[] = []
    for (const field of ['title', 'description', 'figure', 'price']) {
      validations.push(new RequiredFieldValidation(field))
    }
    expect(ValidationComposite).toHaveBeenCalledWith(validations)
  })
})
