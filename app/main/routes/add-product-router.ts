import { Router } from 'express'
import { adaptRoute } from '../adapters/adapter-router'
import { makeAddProduct } from '../factories/add-product-factory'

export default (router: Router): void => {
  router.post('/admin/product', adaptRoute(makeAddProduct()))
}
