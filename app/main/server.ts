import 'reflect-metadata'
import { MysqlConnect } from '../infra/db/mysql/helper/mysql-connect'

MysqlConnect.connect()
  .then(async () => {
    const app = (await import('./config/app')).default
    app.listen(5050, () => console.log('app running in port 5050 🛒🛒🛒'))
  })
  .catch(console.error)
