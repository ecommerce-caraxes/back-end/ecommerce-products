import { MysqlConnect as sut } from './mysql-connect'

describe('MysqlConnect', () => {
  beforeAll(async () => {
    await sut.connect()
  })
  afterAll(async () => {
    await sut.close()
  })
  test('Should connect in mysql correctly', async () => {
    expect(sut.isConnected).toBeTruthy()
  })
})
