import Product from '../entity/product'
import {
  MysqlConnect,
  AddProductModel,
  ProductModel,
  AddProductRepository,
  Repository
} from './product-repository-protocols'

export class ProductRepository implements AddProductRepository {
  private productRepository: Repository<Product>

  constructor() {
    this.productRepository = MysqlConnect.getRepository(Product)
  }

  async add(product: AddProductModel): Promise<ProductModel> {
    const adProduct = this.productRepository.create(product)
    return await this.productRepository.save(adProduct)
  }
}
