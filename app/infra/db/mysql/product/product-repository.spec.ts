import Product from '../entity/product'
import {
  ProductRepository,
  MysqlConnect,
  Repository
} from './product-repository-protocols'

const makeSut = () => {
  return new ProductRepository()
}

let productRepository: Repository<Product>

describe('Product Repository', () => {
  beforeAll(async () => {
    await MysqlConnect.connect()
  })

  afterAll(async () => {
    await MysqlConnect.close()
  })

  beforeEach(async () => {
    productRepository = MysqlConnect.getRepository(Product)
    await productRepository.delete({ title: 'any_title' })
  })

  test('Should return an account on add success', async () => {
    const sut = makeSut()
    const product = await sut.add({
      title: 'any_title',
      description: 'any_description',
      figure: 'any_figure',
      price: 20
    })
    expect(product.id).toBeTruthy()
    expect(product.title).toBeTruthy()
    expect(product.description).toBeTruthy()
    expect(product.figure).toBeTruthy()
    expect(product.price).toBeTruthy()
  })
})
