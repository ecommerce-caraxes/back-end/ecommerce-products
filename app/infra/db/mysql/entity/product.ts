import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('products')
export default class Product {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ type: 'varchar' })
  title: string

  @Column({ type: 'text' })
  description: string

  @Column({ type: 'varchar' })
  figure: string

  @Column({ type: 'float' })
  price: number
}
