import { ProductModel } from '../../model/add-product'

export interface AddProductModel {
  title: string
  description: string
  figure: string
  price: number
}

export interface AddProduct {
  add(product: AddProductModel): Promise<ProductModel>
}
