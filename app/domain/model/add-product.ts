export interface ProductModel {
  id: string
  title: string
  description: string
  figure: string
  price: number
}
