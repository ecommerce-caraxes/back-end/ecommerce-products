import { HttpResponse } from '../protocols/http'

export const badRequest = (error: Error): HttpResponse => ({
  statusCode: 400,
  body: error
})

export const success = (data: any): HttpResponse => ({
  statusCode: 201,
  body: data
})

export const serverError = (error: any) => ({
  statusCode: 500,
  body: error
})
