import {
  AddProduct,
  AddProductModel,
  HttpRequest,
  ProductModel,
  Validation
} from './add-product-protocols'
import { AddProductController } from './add-product-controller'
import { MissingParamError } from '../error/missing-param-field'
import { badRequest } from '../helpers/http-helper'

interface SutTypes {
  sut: AddProductController
  addProductStub: AddProduct
  validationStub: Validation
}

const makeFakeProduct = (): ProductModel => ({
  id: 'any_id',
  title: 'any_title',
  description: 'any_description',
  figure: 'any_figure',
  price: 20
})

const makeFakeRequest = (): HttpRequest => ({
  body: {
    title: 'any_title',
    description: 'any_description',
    figure: 'any_figure',
    price: 'any_price'
  }
})

const makeValidation = (): Validation => {
  class ValidationStub implements Validation {
    validate(input: any): Error {
      return null
    }
  }
  return new ValidationStub()
}

const makeAddProduct = (): AddProduct => {
  class AddProductStub implements AddProduct {
    add(product: AddProductModel): Promise<ProductModel> {
      return new Promise((resolved) => resolved(makeFakeProduct()))
    }
  }
  return new AddProductStub()
}

const makeSut = (): SutTypes => {
  const validationStub = makeValidation()
  const addProductStub = makeAddProduct()
  const sut = new AddProductController(validationStub, addProductStub)
  return {
    sut,
    addProductStub,
    validationStub
  }
}

describe('AddProductController', () => {
  test('Should call Validation with correct value', async () => {
    const { sut, validationStub } = makeSut()
    const spyValidationStub = jest.spyOn(validationStub, 'validate')
    await sut.handle(makeFakeRequest())
    expect(spyValidationStub).toHaveBeenCalledWith(makeFakeRequest().body)
  })
  test('Should return 400 if Validationg returns an error', async () => {
    const { sut, validationStub } = makeSut()
    jest
      .spyOn(validationStub, 'validate')
      .mockReturnValueOnce(new MissingParamError('any_field'))
    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse).toEqual(badRequest(new MissingParamError('any_field')))
  })
  test('Should return an exception if addProduct fails', async () => {
    const { sut, addProductStub } = makeSut()
    jest
      .spyOn(addProductStub, 'add')
      .mockReturnValueOnce(
        new Promise((resolved, reject) =>
          reject(new Error('internal server error'))
        )
      )
    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new Error('internal server error'))
  })
  test('Should call addProduct with the correct values', async () => {
    const { sut, addProductStub } = makeSut()
    const addProductSpy = jest.spyOn(addProductStub, 'add')
    await sut.handle(makeFakeRequest())
    expect(addProductSpy).toHaveBeenCalledWith(makeFakeRequest().body)
  })
  test('Should return 201 if addProduct is a success', async () => {
    const { sut } = makeSut()
    const httpResponse = await sut.handle(makeFakeRequest())
    expect(httpResponse.statusCode).toBe(201)
    expect(httpResponse.body).toEqual(makeFakeProduct())
  })
})
