import { AddProductModel } from '../../domain/usecases/add-product/add-product'
import { ProductModel } from '../usecases/add-product/add-product-protocols'

export interface AddProductRepository {
  add(product: AddProductModel): Promise<ProductModel>
}
