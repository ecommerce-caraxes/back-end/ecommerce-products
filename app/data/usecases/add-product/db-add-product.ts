import {
  AddProductRepository,
  AddProductModel,
  ProductModel,
  AddProduct
} from './add-product-protocols'

export class DbAddProduct implements AddProduct {
  constructor(private readonly addProductRepository: AddProductRepository) {}

  async add(addProduct: AddProductModel): Promise<ProductModel> {
    const product = await this.addProductRepository.add(addProduct)
    return product
  }
}
