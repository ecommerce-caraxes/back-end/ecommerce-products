import {
  AddProductRepository,
  AddProductModel,
  ProductModel
} from './add-product-protocols'
import { DbAddProduct } from './db-add-product'

interface SutTypes {
  sut: DbAddProduct
  addProductRepositoryStub: AddProductRepository
}

const makeFakeRequest = () => ({
  title: 'any_title',
  description: 'any_description',
  figure: 'any_figure',
  price: 20
})

const makeFakeResponse = () => ({
  id: 'any_id',
  title: 'any_title',
  description: 'any_description',
  figure: 'any_figure',
  price: 20
})

const addProductStub = (): AddProductRepository => {
  class AddProductRepositoryStub implements AddProductRepository {
    async add(addProduct: AddProductModel): Promise<ProductModel> {
      return new Promise((resolved) => resolved(makeFakeResponse()))
    }
  }
  return new AddProductRepositoryStub()
}

const makeSut = (): SutTypes => {
  const addProductRepositoryStub = addProductStub()
  const sut = new DbAddProduct(addProductRepositoryStub)
  return {
    sut,
    addProductRepositoryStub
  }
}

describe('DbAddProduct', () => {
  test('Should call AddProductRepository how correct values', async () => {
    const { sut, addProductRepositoryStub } = makeSut()
    const spyAddProductRepository = jest.spyOn(addProductRepositoryStub, 'add')
    await sut.add(makeFakeRequest())
    expect(spyAddProductRepository).toHaveBeenCalledWith(makeFakeRequest())
  })
  test('Should call exception se addProductRepository fail', async () => {
    const { sut, addProductRepositoryStub } = makeSut()
    jest
      .spyOn(addProductRepositoryStub, 'add')
      .mockReturnValue(new Promise((resolved, reject) => reject(new Error(''))))
    const promise = sut.add(makeFakeRequest())
    await expect(promise).rejects.toThrow()
  })
  test('Should return an product if on success', async () => {
    const { sut } = makeSut()
    const promise = await sut.add(makeFakeRequest())
    expect(promise).toEqual(makeFakeResponse())
  })
})
